$(document).ready(function () {

  if (typeof window.spoof_qwerty == 'undefined' ) {
    $('#spoof-overlay').fadeIn('slow');
  }


  $(".bitcoin-bonus").click(function () {

    var ansBlock = $("#selected-bitcoin");

    $.ajax({
      url: '/game/getlotery',
      type: 'POST',
      data: ({'numberBitcoin': $(this).attr('alt'), 'rate': $('#rate').val()}),
      dataType: "html",
      beforeSend: function () {
        $("#selected-bitcoin").text("Отправка данных...");
      },
      success: function (data) {
        var answer = JSON.parse(data);
        if (answer[1] === 'no_many') {
          ansBlock.text('Извините, у вас недостаточно средств на балансе.');
        }
        else if (answer[1] === 'small_bet') {
          ansBlock.text('Ваша ставка меньше двух рублей');
        }
        else if (answer[1] === 'win') {
          ansBlock.text('Поздравляю, ты выиграл ' + $('#rate').val() + 'руб.');
        } else {
          ansBlock.text('Извини, выигрышный сундук был под номером ' + answer[0]);
        }
      },
      error: function () {
        ansBlock.text('Ошика соединения (лотерея)');
      }
    });

    funcSuccess('#selected-bitcoin', 1000, 1000, 4000);

  });

  $(".get-bonus-button").click(function () {

    var form = jQuery("#bonus-form");

    $.ajax({
      url: '/site/bonus',
      type: 'POST',
      data: form.serialize(),
      dataType: "html",
      beforeSend: function () {
        $("#timer").text("Подождите...");
      }, // функция которая выполняется до ответа сервера
      success: function (data) {
        $("#timer").text(data);
        grecaptcha.reset();
      },
      error: function () {
        $("#timer").text('Ошика соединения');
      }
    });

    funcSuccess('#timer', 1000, 1000, 4000);

  });

  $("#send_to_support").click(function () {

    var cause = $('#support-cause').val();
    var text = $('#support-t').val();
    // alert(cause);
    // alert(text);

    $.ajax({
      url: '/user/sendsupport',
      type: 'POST',
      data: ({'cause': cause, 'text': text}),
      dataType: 'html',
      success: function (data) {
        $('#support-answer-block').text(data);
      },
      error: function (data) {
        $('#support-answer-block').text(data);
      },
    });

  });

  function funcSuccess($elem, $in, $out, $time) {
    $($elem).fadeIn($in);
    setTimeout(function () {
      $($elem).fadeOut($out);
    }, $time);
  }


//******************************************************   ПОПАП ОКНО   **********************************************

// OPEN THE BUTTON
  $(".js-button-campaign").click(function (event) {
    event.preventDefault();
    $(".js-overlay-campaign").fadeIn();
    $(".js-overlay-campaign").addClass('disabled');
  });

// CLOSE UP THE CROSS
  $('.js-close-campaign').click(function () {
    $('.js-overlay-campaign').fadeOut();
  });
// NO BUTTON
  $('.no-delete-user').click(function () {
    $('.js-overlay-campaign').fadeOut();
  });

// закрыть по клику вне окна
  $(document).mouseup(function (e) {
    var popup = $('.js-popup-campaign');
    if (e.target != popup[0] && popup.has(e.target).length === 0) {
      $('.js-overlay-campaign').fadeOut();

    }
  });


//******************************************************   ПРОЧЕЕ   **********************************************

});