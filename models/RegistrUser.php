<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 16.08.2017
 * Time: 23:26
 */

namespace app\models;
use yii\db\ActiveRecord;

class RegistrUser extends ActiveRecord
{

  public static function tableName()
  {
    return 'users';
  }


  public function attributeLabels() {
    return [
      'email' => 'Email',
      'username' => 'Login',
      'wallet' => 'Кошелек',
      'password' => 'Пароль',
    ];
  }

//  правила валидации
  public function rules() {
    return [
      [['email', 'username','wallet', 'password'], 'required'],
      ['email', 'email'],
      ['email', 'unique', 'targetClass' => 'app\models\User'],
      ['wallet', 'match', 'pattern' => '/^P[0-9]{8}$/'],
    ];
  }

  public function signup() {
    $user = new RegistrUser();
    $user->email = $this->email;
    $user->username = $this->username;
    $user->wallet = $this->wallet;
    $user->password = sha1($this->password);
    return $user->save();
  }

}