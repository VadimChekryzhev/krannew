<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $auth_key
 * @property string $email
 * @property string $username
 * @property string $wallet
 * @property string $password
 * @property double $balance
 * @property string $ip
 * @property integer $protect
 * @property integer $total
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'wallet'], 'required'],
            [['balance'], 'number'],
            [['protect', 'total'], 'integer'],
            [['auth_key', 'email', 'wallet', 'password', 'ip'], 'string', 'max' => 255],
            [['username'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'email' => 'Email',
            'username' => 'Username',
            'wallet' => 'Wallet',
            'password' => 'Password',
            'balance' => 'Balance',
            'ip' => 'Ip',
            'protect' => 'Protect',
            'total' => 'Total',
        ];
    }
}
