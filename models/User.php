<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{

  public static function tableName() {
    return 'users';
  }

  /**
   * @inheritdoc
   */
  public static function findIdentity($id) {
    return static::findOne($id);
  }

  /**
   * @inheritdoc
   */
  public static function findIdentityByAccessToken($token, $type = null) {
//    return static::findOne(['access_token' => $token]);
  }

  /**
   * Finds user by email
   *
   * @param string $email
   * @return static|null
   */
  public static function findByEmail($email) {
    return static::findOne(['email' => $email]);
  }

//  public static function findByWallet($wallet) {
//    return static::findOne(['wallet' => $wallet]);
//  }

  /**
   * @inheritdoc
   */
  public function getId() {
    return $this->id;
  }

  public function getUsername() {
    return $this->username;
  }

  /**
   * @inheritdoc
   */
  public function getAuthKey()  {
    return $this->auth_key;
  }

  /**
   * @inheritdoc
   */
  public function validateAuthKey($authKey) {
    return $this->auth_key === $authKey;
  }

  /**
   * Validates password
   *
   * @param string $password password to validate
   * @return bool if password provided is valid for current user
   */

  public function validatePassword($password) {
//    return true;
    return $this->password == sha1($password);
//    return Yii::$app->security->validatePassword($password, $this->password);
  }



  public function generateAuthKey() {
    $this->auth_key = Yii::$app->security->generateRandomString();
  }


}
