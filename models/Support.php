<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 16.09.2017
 * Time: 16:01
 */

namespace app\models;

use yii\db\ActiveRecord;

class Support extends ActiveRecord
{

//  public $cause;
//  public $text;

  public static function tableName()
  {
    return 'support';
  }

  public function attributeLabels()
  {
    return [
      'cause' => 'Проблема:',
      'text' => 'Описание проблемы',
    ];
  }

  public function rules()
  {
    return [
      [['cause', 'text'], 'required'],
    ];
  }

}