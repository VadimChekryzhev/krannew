<?php

use yii\db\Migration;

/**
 * Handles the creation of table `support`.
 */
class m170918_121035_create_support_table extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('support', [
      'id' => $this->primaryKey(),
      'id_user' => $this->integer(),
      'cause' => $this->integer(),
      'text' => $this->string(),
      'decision' => $this->integer(),
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('support');
  }
}
