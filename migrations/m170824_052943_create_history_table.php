<?php

use yii\db\Migration;

/**
 * Handles the creation of table `history`.
 */
class m170824_052943_create_history_table extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('history', [
      'id' => $this->primaryKey(),
      'id_user' => $this->integer(),
      'date' => $this->dateTime(),
      'bonus' => $this->string(),
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('history');
  }
}
