<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payment`.
 */
class m170914_074832_create_payment_table extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('payment', [
      'id' => $this->primaryKey(),
      'id_user' => $this->integer(),
      'date' => $this->dateTime(),
      'pay' => $this->string(),
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('payment');
  }
}
