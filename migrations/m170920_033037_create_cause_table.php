<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cause`.
 */
class m170920_033037_create_cause_table extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('cause', [
      'id' => $this->primaryKey(),
      'cause' => $this->integer()->defaultValue(0),
      'description' => $this->string(),
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('cause');
  }
}
