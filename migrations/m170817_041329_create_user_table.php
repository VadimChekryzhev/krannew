<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170817_041329_create_user_table extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('users', [
      'id' => $this->primaryKey(),
      'auth_key' => $this->string()->defaultValue(0),
      'email' => $this->string()->defaultValue(0),
      'username' => $this->string(15),
      'wallet' => $this->string()->defaultValue(0),
      'password' => $this->string(),
      'balance' => $this->float()->defaultValue(0),
      'ip' => $this->string()->defaultValue(0),
//      On/Off protect IP
      'protect' => $this->integer(1)->defaultValue(0),
//      payments counter
      'total' => $this->integer()->defaultValue(0),
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('user');
  }
}
