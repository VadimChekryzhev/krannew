<script src="https://www.google.com/recaptcha/api.js"></script>

<?php
  use yii\widgets\ActiveForm;
  use yii\helpers\Html;
?>

<?php if (Yii::$app->session->hasFlash('success')) : ?>
    <div class="alert alert-success alert-dismissable alert-registr">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?= Yii::$app->session->getFlash('success') ?>
    </div>

<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('error')) : ?>
  <div class="alert alert-danger alert-dismissable alert-registr">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?= Yii::$app->session->getFlash('error') ?>
  </div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('warning')) : ?>
  <div class="alert alert-warning alert-dismissable alert-registr">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?= Yii::$app->session->getFlash('warning') ?>
  </div>
<?php endif; ?>

<h1>Регистрация</h1>


<div class="form">
  <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal', 'id' => 'registrUser']]); ?>
  <div class="login_email">
    <div class="login_icon">
      <i class="fa fa-envelope" aria-hidden="true"></i>
    </div>
    <?= $form->field($user, 'email')->input('email', ['placeholder' => "Email"])->label(false) ?>
  </div>

  <?= $form->field($user, 'username')->input( 'text', ['placeholder' => "Username"])->label(false) ?>

  <?= $form->field($user, 'password')->passwordInput()->input( 'password', ['placeholder' => "Password"])->label(false) ?>

  <?= $form->field($user, 'wallet')->input( 'text', ['placeholder' => "Wallet: P12345678"])->label(false) ?>

  <div class="form-group">
    <div class="col-lg-12 text-center">
      <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary custom-button', 'name' => 'login-button']) ?>
    </div>
  </div>
</div>


<?php ActiveForm::end(); ?>