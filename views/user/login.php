<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
  <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
      'id' => 'login-form',
      'class' => 'form-horizontal',
//      'layout' => 'horizontal',
//      'fieldConfig' => [
//        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
//        'labelOptions' => ['class' => 'col-lg-1 control-label'],
//      ],
    ]); ?>

    <?= $form->field($model, 'email')->input('email', ['placeholder' => "Email"])->label(false) ?>

    <?= $form->field($model, 'password')->passwordInput()->input( 'password', ['placeholder' => "Password"])->label(false) ?>

    <?= $form->field($model, 'rememberMe')->checkbox([
      'template' => "<div class=\"col-lg-12 rememberMe\">{input} {label}</div>\n<div class=\"col-lg-12\">{error}</div>",
//      'template' => "<div class=\"col-lg-offset-1 col-lg-4\">{input} {label}</div>\n<div class=\"col-lg-7\">{error}</div>",
    ]) ?>

    <div class="form-group">
      <div class="col-lg-12 text-center">
        <?= Html::submitButton('Войти', ['class' => 'btn btn-primary custom-button', 'name' => 'login-button']) ?>
      </div>
    </div>
    <?php ActiveForm::end(); ?>


</div>
