<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<h1>Служба поддержки</h1>

<div class="form-support">
  <?php $form = ActiveForm::begin([
    'id' => 'support-form',
    'class' => 'support-form',
  ]); ?>

  <?= $form->field($support, 'cause')->dropDownList(
    [
      '0' => '',
      '1' => 'Выплата не пришла',
      '2' => 'Не работает лотерея',
      '3' => 'Проблема с капчей',
      '4' => 'Прочее',
    ]
  )->label(true);
  ?>



  <?= $form->field($support, 'text')->textarea(['id' => 'support-t', 'rows' => '7']); ?>

<!--  <div class="col-md-12 text-center clearfix">-->
<!--    <div class="row">-->
      <?= Html::button('Отправить', ['class' => 'btn btn-primary custom-button', 'name' => 'support-button', 'id' => 'send_to_support']) ?>
<!--    </div>-->
<!--  </div>-->
</div>


<div id="support-answer-block"></div>


<?php ActiveForm::end(); ?>
