<?php

use yii\helpers\Html;

?>

<h1><?= $username ?></h1>
<p>Текущий баланс: <?= $balance ?> руб.</p>
<p>Получено бонусов: <?= $total ?></p>
<p>До выплаты осталось: <?= $remainder ?> руб.</p>

<?= Html::a('Заказать детализацию выплат', ['/user/pdf'], ['class' => 'custom-button long-button'])?>
<?= Html::a('Редактировать аккаунт', ['#'], ['class' => 'custom-button long-button'])?>
<?= Html::a('Написать в поддержку', ['/user/support'], ['class' => 'custom-button long-button write-support'])?>
<?= Html::a('Удалить аккаунт', ['#'], ['class' => 'custom-button long-button js-button-campaign'])?>

<?//= Html::button('Заказать детализацию выплат', ['class' => 'custom-button long-button', 'id' => 'detailing']) ?>
<?//= Html::button('Редактировать аккаунт', ['class' => 'custom-button long-button', 'id' => 'edit-akk']) ?>
<?//= Html::button('Написать в поддержку', ['class' => 'custom-button long-button', 'id' => 'write-support']) ?>
<?//= Html::button('Удалить аккаунт', ['class' => 'custom-button long-button js-button-campaign', 'id' => 'delete-akk']) ?>


<div class="overlay js-overlay-campaign">
  <div class="delete-popup popup js-popup-campaign">
    <h3>Удалить аккаунт?</h3>
    <p>Вы уверенны что хотите удалить аккаунт? Все невыплаченые средства пропадут!</p>
    <form action="site/delete">
      <?= Html::button('Да', ['class' => 'custom-button delete-user-button']) ?>
      <?= Html::button('Нет', ['class' => 'custom-button no-delete-user']) ?>
    </form>
    <div class="close-popup js-close-campaign"></div>
  </div>
</div>
