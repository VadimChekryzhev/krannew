<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>


<h1>Лотерея для нормальных пацанов</h1>

<div class="regulations-lotery">
  <p>Готов испытать свою удачу?</p>
  <div class="form-group  ">
    <input type="text" id="rate" value="" placeholder="2 руб.">
  </div>
  <p class="under-text">Поставь любую сумму, но не меньше 2 рублей и при выигрыше твоя ставка удвоится!</p>
</div>


<div class="container-fluid lotery-container">
  <div class="row">
    <?= Html::img('@web/img/bitcoin.jpg', ['alt' => '1', 'class' => 'bitcoin-bonus col-md-3 col-xs-6']) ?>
    <?= Html::img('@web/img/bitcoin.jpg', ['alt' => '2', 'class' => 'bitcoin-bonus col-md-3 col-xs-6']) ?>
    <?= Html::img('@web/img/bitcoin.jpg', ['alt' => '3', 'class' => 'bitcoin-bonus col-md-3 col-xs-6']) ?>
    <?= Html::img('@web/img/bitcoin.jpg', ['alt' => '4', 'class' => 'bitcoin-bonus col-md-3 col-xs-6']) ?>
  </div>
</div>
<div class="container-fluid lotery-container">
  <div class="row">
    <?= Html::img('@web/img/bitcoin.jpg', ['alt' => '5', 'class' => 'bitcoin-bonus col-md-3 col-xs-6']) ?>
    <?= Html::img('@web/img/bitcoin.jpg', ['alt' => '6', 'class' => 'bitcoin-bonus col-md-3 col-xs-6']) ?>
    <?= Html::img('@web/img/bitcoin.jpg', ['alt' => '7', 'class' => 'bitcoin-bonus col-md-3 col-xs-6']) ?>
    <?= Html::img('@web/img/bitcoin.jpg', ['alt' => '8', 'class' => 'bitcoin-bonus col-md-3 col-xs-6']) ?>
  </div>
</div>

<div id="selected-bitcoin"></div>
