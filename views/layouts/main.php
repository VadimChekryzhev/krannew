<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
  (function (d, w, c) {
    (w[c] = w[c] || []).push(function () {
      try {
        w.yaCounter45840108 = new Ya.Metrika({
          id: 45840108,
          clickmap: true,
          trackLinks: true,
          accurateTrackBounce: true,
          webvisor: true,
          trackHash: true
        });
      } catch (e) {
      }
    });

    var n = d.getElementsByTagName("script")[0],
      s = d.createElement("script"),
      f = function () {
        n.parentNode.insertBefore(s, n);
      };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
      d.addEventListener("DOMContentLoaded", f, false);
    } else {
      f();
    }
  })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
  <div><img src="https://mc.yandex.ru/watch/45840108" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta name="google-site-verification" content="M4S4I50MYIEDP-eKANr3GgUD5jvpYTj9p98cTZsrrRA"/>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>
<!-- ADBLOCK AND ADGUART -->
<div id="spoof-overlay">
  <p>Выключите пожалуйства AdBlock или AdGuart что бы получить доступ к сайту!</p>
</div>

<div class="wrapper">

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wrap top-menu">
          <ul class="nav nav-pills nav-justified top-menu">
            <li role="presentation"><?= Html::a('Главная', ['/site/index']) ?></li>
            <?php if (!Yii::$app->user->isGuest): ?>
              <li role="presentation"><?= Html::a('Удвой баланс', ['game/lotery']) ?></li>
              <li role="presentation"><?= Html::a('Профиль', ['/user/profile']) ?></li>
              <!--            <li role="presentation">--><? //= Html::a('Выплаты', ['pay/index']) ?><!--</li>-->
              <li role="presentation"><?= Html::a('Выход', ['/user/logout']) ?></li>
            <?php else: ?>
              <li role="presentation"><?= Html::a('Вход', ['user/login']) ?></li>
              <li role="presentation"><?= Html::a('Регистрация', ['user/registr']) ?></li>
            <?php endif; ?>
            <?php if (Yii::$app->user->can('canAdmin')): ?>
              <li role="presentation"><?= Html::a('Admin', ['/admin']) ?></li>
            <?php endif; ?>
            <!--            <li role="presentation">--><? //= Html::a('Поддержка', ['support/support']) ?><!--</li>-->
          </ul>
        </div>
      </div>
    </div>
  </div>


  <div class="container">
    <div class="row">
      <div class=" col-md-3">
        <div class="slide">
          <div class="spoof">
            <a href="https://payeer.com/?partner=4900132" target="_blank"><img
                  src="https://payeer.com/bitrix/templates/difiz/img/banner/ru/250x250.gif" alt=""></a>
            <!--            <p>Место для рекламы</p>-->
          </div>
          <div class="spoof">
            <a href="http://bitincome.io/?ref=whoimi" target="_blank"><img
                  src="http://bitincome.io/img/promo/ru/160x600.gif" alt=""></a>
            <!--            <p>Место для рекламы</p>-->
          </div>
          <div class="spoof">
            <!--            <p>Место для рекламы</p>-->
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="content">

          <?= $content ?>

        </div>
      </div>

      <div class=" col-md-3">
        <div class="slide">
          <div class="spoof">
            <a target="_blank" href="https://bitcoset.com/?r=19817"><img src="http://bitcoset.com/images/banners/160x600.gif" alt="Bitcoset Ads" title="Bitcoset Ads" width="160" height="600" border="0" /></a>
            <!--            <p>Место для рекламы</p>-->
          </div>
          <div class="spoof">
            <a href="https://coinmedia.co?ref=14273" title="coinmedia.co"><img src="https://coinmedia.co/img/Banner_200x200.gif" alt="coinmedia.co"></a>
            <!--            <p>Место для рекламы</p>-->
          </div>
          <div class="spoof">
            <!--            <p>Место для рекламы</p>-->
          </div>
        </div>
      </div>
    </div>

    <footer>
      <div class="colum-footer">
        <div class="logo">FreeBie</div>
      </div>
      <div class="colum-footer">
        <a href="http://bitincome.io/?ref=whoimi">Получай от 7% до 13% ежедневно!</a>
      </div>
      <div class="colum-footer">РЕКЛАМНОЕ МЕСТО ПУСТО</div>
    </footer>

  </div>

  <!-- BEGIN JIVOSITE CODE {literal} -->
  <script type='text/javascript'>
    (function () {
      var widget_id = 'oYDf1f9scL';
      var d = document;
      var w = window;

      function l() {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = '//code.jivosite.com/script/widget/' + widget_id;
        var ss = document.getElementsByTagName('script')[0];
        ss.parentNode.insertBefore(s, ss);
      }

      if (d.readyState == 'complete') {
        l();
      } else {
        if (w.attachEvent) {
          w.attachEvent('onload', l);
        } else {
          w.addEventListener('load', l, false);
        }
      }
    })();</script>
  <!-- {/literal} END JIVOSITE CODE -->

  <?php $this->endBody() ?>
</div>
</body>
</html>
<?php $this->endPage() ?>
