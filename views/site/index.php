<?php

use yii\helpers\Html;

?>

<script src="https://www.google.com/recaptcha/api.js"></script>
<div class="form form-index">
  <h1>Получи бонус</h1>
  <p class="bonus-text">Получи бесплатно бонус до 1 руб.</p>
  <form action="" id="bonus-form">
    <div id="recaptcha">
      <div class="g-recaptcha" data-sitekey="<?= PUBLIC_RECAPCHA ?>"></div>
    </div>
    <div>
      <?= Html::button('получить', ['class' => 'custom-button get-bonus-button'] ) ?>
    </div>
  </form>
  <div id="timer"><?= $response ?></div>

</div>

<div class="long-banner">
  <a href="https://www.bithaul.net/?ref=3273" alt="BitHaul Bitcoin Investing"><img src="https://www.bithaul.net/assets/images/banner/b2.gif"></a></div>

<h1 class="mt">ТОП 5 самых активных пользователей</h1>
<table>
  <tr>
    <th>Псевдоним</th>
    <th>Получено бонусов</th>
  </tr>
  <?= $active_str ?>
</table>

<div class="long-banner">
  <a target="_blank" href="https://bitcoset.com/?r=19817"><img src="http://bitcoset.com/images/banners/468x60.gif" alt="Bitcoset Ads" title="Bitcoset Ads" width="468" height="60" border="0" /></a></div>

<h1 class="mt">Последние 10 выплат</h1>
<table>
  <tr>
    <th>Дата</th>
    <th>Псевдоним</th>
    <th>Сумма, руб.</th>
  </tr>
  <?= $last_payment_str ?>
</table>