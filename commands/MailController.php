<?php
namespace app\commands;

use yii\console\Controller;
use Yii;

class MailController extends Controller
{

  public function actionTest($message = 'working')
  {
    echo $message. "\n";
  }

  public function actionMail()
  {
    Yii::$app->mailer->compose()
      ->setFrom('wadim199602@gmail.com')
      ->setTo('wadim199602@gmail.com')
      ->setSubject('Тема сообщения')
      ->setTextBody('Текст сообщения')
      ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
      ->send();
    echo 'Success';
  }

}