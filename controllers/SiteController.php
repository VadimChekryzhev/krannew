<?php

namespace app\controllers;

use app\models\History;
use app\models\Payment;
use app\models\User;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class SiteController extends AppController
{

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['logout', 'profile'],
        'rules' => [
          [
            'actions' => ['logout', 'profile'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['post', 'get'],
        ],
      ],
    ];
  }


  /**
   * Home Page
   * @return string
   */
  public function actionIndex()
  {
    $this->view->title = 'Главная';
    $active = User::find()->asArray()->limit(5)->orderBy(['total' => SORT_DESC])->all();
    $last_payment = History::find()->asArray()->limit(10)->orderBy(['date' => SORT_DESC])->all();
    $active_str = $last_payment_str = '';

    foreach ($active as $row) {
      $active_str .= '<tr><td>' . $row['username'] . '</td><td>' . $row['total'] . '</td></tr>';
    }

    foreach ($last_payment as $row) {
      $last_payment_str .= '<tr><td>' . $row['date'] . '</td><td>' . $row['id_user'] . '</td><td>' . r2f($row['bonus']) . '</tr>';
    }

    return $this->render('index', compact('active_str', 'last_payment_str'));
  }


  /**
   * Get Bonus In Home Page
   * Valid Capcha
   * $period - time since last payment
   *
   */
  public function actionBonus()
  {

    /**
     * Answer for the user
     */
    $answer = '';

    if (!$_POST['g-recaptcha-response']) {
      echo('Капча введена неверно 1');
      exit();
    }

    $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . SECRET_RECAPCHA . '&response=' . $_POST['g-recaptcha-response'] . '&remoteip=' . $_SERVER['REMOTE_ADDR'];
    $data = json_decode(file_get_contents($url));
    if ($data->success == false) {
      echo('Капча введена неверно 2');
      exit();
    }


    if (!Yii::$app->user->isGuest) {
      $data = History::find()->where(['id_user' => Yii::$app->user->identity->getId()])->orderBy(['date' => SORT_DESC])->one();
      $limit = strtotime($data['date']) + PAY_TIME;
      $time = time();
      $period = $limit - $time;

      if ($period < 0) {
        /**
         * Render in function
         */
        $bonus = round(mt_rand(MIN_BONUS, MAX_BONUS) / mt_rand((MIN_BONUS * 5), (MAX_BONUS * 5)), 2);
        $user = Users::findOne(Yii::$app->user->identity->getId());

        /**
         * History - bad name for the table
         */
        $pay = new History();
        $pay->id_user = $user->id;
        $pay->date = date("Y-m-d H:i:s");
        $pay->bonus = $bonus;

        $user->balance = $user->balance + $bonus;
        $user->total = $user->total + 1;

        /**
         * Возможно что сохранение на данном этапе выглядит нелогично,
         * опирался на то, что бы дальнейшая ошибка в скрипте не вызвала
         * потерю полученного пользователем бонуса
         */
        $transaction = Yii::$app->db->beginTransaction();
        try {
          if ($user->save() && $pay->save()) {
            $transaction->commit();
            $answer = 'Вы получили: ' . $bonus . ' руб.';
          }
        } catch (\Throwable $e) {
          $transaction->rollBack();
        }

//***************************************************************************************************

        if ($user->balance >= PAYMENT_LIMIT) {
          $accountNumber = PAYEER_WALLET;
          $apiId = PAYEER_ID;
          $apiKey = PAYEER_KEY;
          $payeer = new \CPayeer($accountNumber, $apiId, $apiKey);

          if ($payeer->isAuth()) {
            $arTransfer = $payeer->transfer(array(
              'curIn' => 'RUB',
              'sum' => $user->balance,
              'curOut' => 'RUB',
              'to' => $user->wallet,

              /**
               * Wallet for debugging @to 'P73545823'
               */

              'comment' => 'Платеж с сайта whoimi.ru',
            ));

            if (empty($arTransfer['errors'])) {

              /**
               * @payment to Payment table
               */
              $payment = new Payment();
              $payment->id_user = $user->id;
              $payment->date = date("Y-m-d H:i:s");
              $payment->pay = $user->balance;

              /**
               * Payment of funds
               */
              $transaction = Yii::$app->db->beginTransaction();
              try {
                if ($user->save() && $payment->save()) {
                  $user->balance = 0;
                  $transaction->commit();
//                  $answer .= ".   Перевод средств успешно выполнен: " . $arTransfer['historyId'];
//                  $answer .= "Вы получили ". $bonus . " руб. Произведена автоматическая выплата средств";
                  echo "Вы получили " . $bonus . " руб. Произведена автоматическая выплата средств";
                  exit();
                }
              } catch (\Throwable $e) {
                $transaction->rollBack();
              }

            } else {
              echo 'Ошибка при переводе средств на Ваш счет. Обратитесь в службу поддержки';
              exit();
//              echo '<pre>' . print_r($arTransfer['errors'], true) . '</pre>';
            }
          } else {
            echo 'Нет связи с сервером. Просьба сообщить об этом в службу поддержки';
            exit();
//            echo '<pre>' . print_r($payeer->getErrors(), true) . '</pre>';
          }
        }

//***************************************************************************************************

      } else {
//        $answer = 'Для получения бонуса необходимо подождать ещё ' . $period . ' сек.'; //' . $limit - $time . '
        echo 'Для получения бонуса необходимо подождать ещё ' . $period . ' сек.'; //' . $limit - $time . '
        exit();
      }
    } else {
//      $answer = 'Для получения бонуса необходимо авторизоваться на сайте!';
      echo 'Для получения бонуса необходимо авторизоваться на сайте!';
      exit();
    }

    echo $answer;
  }

}
