<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 25.08.2017
 * Time: 22:34
 */

namespace app\controllers;

use app\models\Payment;
use app\models\User;
use app\models\Users;
use app\models\Support;
use Yii;
use app\models\LoginForm;
use app\models\RegistrUser;

class UserController extends AppController
{

  /**
   * User Authorization
   * @return string|\yii\web\Response
   */
  public function actionLogin()
  {
    $this->view->title = 'Войти на сайт';

    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    }

    return $this->render('login', [
      'model' => $model,
    ]);
  }


  public function actionRegistr()
  {
    $this->view->title = 'Регистрация';
    $user = new RegistrUser();

    if ($user->load(Yii::$app->request->post())) {
      if ($user->validate() && $user->signup()) {

//        $auth = Yii::$app->authManager;
//        $authorRole = $auth->getRole('user');
//        $auth->assign($authorRole, $user->getId());

        Yii::$app->session->setFlash('success', 'Регистрация успешно завершена. Регистрационные данные высланы на ваш email');

        $to = $user->email;

        $subject = "Поздравляем с регистрацией";

        $message = '<p>Поздравляем с регистрацией на сайте whoimi.ru</p>';
        $message .= '<p>Ваш логин: '. $user->username . '</p>';
        $message .= '<p>Ваш пароль: '. $user->password. '</p>';


        $headers  = "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= "From: support@whoimi.ru <support@whoimi.ru>\r\n";
        $headers .= "Reply-To: support@whoimi.ru\r\n";

        mail($to, $subject, $message, $headers);

        return $this->refresh();
      } else {
        Yii::$app->session->setFlash('error', 'Ошибка регистрации');
      }
    } else {
    }

    return $this->render('registr', compact('user'));
  }


  public function actionLogout()
  {
    Yii::$app->user->logout();
    return $this->goHome();
  }


  public function actionDeluser()
  {
    $id = Yii::$app->user->getId();

    Yii::$app->user->logout();
    User::deleteAll('id = :id', [':id' => $id]);

    return $this->goHome();
  }


  public function actionProfile()
  {
    $user = Yii::$app->user->identity;
    $username = $user['username'];
    $balance = r2f($user['balance']);
    $total = $user['total'];
    $remainder = r2f(PAYMENT_LIMIT - $balance);

    return $this->render('profile', compact('username', 'balance', 'total', 'remainder'));
  }


  public function actionSupport()
  {

    $this->view->title = 'Служба поддержки';

    $support = new Support();

    return $this->render('support', compact('support'));
  }

  public function actionSendsupport()
  {
    $answer = '';

    $support = new Support();

    $cause = $_POST['cause'];
    $text = $_POST['text'];
    if ($cause && $text) {
      $support->cause = $cause;
      $support->text = $text;

      if ($support->validate()) {

        $support->id_user = Yii::$app->user->id;
        $support->decision = 0;
        $support->save();
        $answer .= "Ваша заявка будет обработана в ближайшее время";
      } else {
        $answer .= "Validate Error";
      }
    } else {
      $answer .= "Необходимо заполнить все поля формы";
    }

    echo $answer;
  }

  public function actionPdf()
  {
    $user = Yii::$app->user->identity;
    $username = $user['username'];
    $balance = r2f($user['balance']);
    $id = $user['id'];

    $pay = Payment::find()
      ->select(['date', 'pay'])
      ->asArray()
      ->where(['id_user' => $id])
      ->limit(20)
      ->all();


    $mpdf = new \mPDF('utf-8', 'A4', '10', '', 10, 10, 7, 7, 10, 10);
//    CSS
    $stylesheet = file_get_contents('../statement/statement.css');
    $mpdf->WriteHTML($stylesheet, 1);

//    ***********************************************************************************
    $html = '<h1>Детализация выплат: ' . $username . '</h1>';
    $html .= '<p class="balance">Баланс на ' . date("y-m-d H:i") . ': ' . $balance . ' руб.</p>';

    $table = '<table><caption>Последние 20 выплат</caption><tr><th>№</th><th>Сумма выплаты (руб)</th><th>Дата выплаты</th></tr>';
    $i = 1;
    $sum = 0;
    if (empty($pay)) {
      $table .= '<tr><td>$i</td><td>n/a</td><td>n/a</td></tr>';
    } else {
      foreach ($pay as $value) {
        $table .= "<tr><td>$i</td><td>$value[pay]</td><td>$value[date]</td></tr>";
        $sum += $value['pay'];
        $i++;
      }
    }

    $table .= '</table>';
    $html .= $table;

    $html .= '<p>За все время получено ' . $sum . ' руб.</p>';
//    ***********************************************************************************
    $mpdf->WriteHTML($html, 2);
    $mpdf->Output('mpdf.pdf', 'I');
  }

}