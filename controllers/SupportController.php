<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 25.08.2017
 * Time: 22:23
 */

namespace app\controllers;


class SupportController extends AppController
{

  public function actionSupport()
  {
    return $this->render('support');
  }

}