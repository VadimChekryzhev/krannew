<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 20.08.2017
 * Time: 14:03
 */

namespace app\controllers;

require_once '../config/cpayeer.php';

use yii\web\Controller;

class AppController extends Controller
{
  
  public function debug($arr) {
    echo '<pre>'. print_r($arr, true) .'</pre>';
  }

}


/**
 * @param $arr - объект, который необходимо распечатать более читабельно
 */
function debug($arr) {
  echo '<pre>'. print_r($arr, true) .'</pre>';
}


/**
 * @param $num
 * @return $num - float decomals 2
 */
function r2f($num) {
  return number_format((float)$num, 2, '.', '');
}