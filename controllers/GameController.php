<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class GameController extends AppController
{


  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['lotery'],
        'rules' => [
          [
            'actions' => ['lotery'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['post', 'get'],
        ],
      ],
    ];
  }


  /**
   * @return string
   */
  public function actionLotery()
  {
    return $this->render('lotery');
  }


  /**
   * @return string - result of the game
   */
  public function actionGetlotery()
  {
    $user = Users::findOne(Yii::$app->user->identity->getId());

    $rate = +$_POST['rate'];
    $rand = mt_rand(1, 8);
    $data[0] = $rand;

    if ($user->balance - $rate > 0) {
      if ($rate >= 2) {
        if ($_POST['numberBitcoin'] != $rand) {
          $data[1] = 'no_win';
          $user->balance = $user->balance - $rate;
        } else {
          $data[1] = 'win';
          $user->balance = $user->balance + $rate * 2;
        }
        $user->save();
      } else {
        $data[1] = 'small_bet';
      }
    } else {
      $data[1] = 'no_many';
    }

    $data = json_encode($data);
    echo $data;
  }

//  public function actionRole() {
//    $admin = Yii::$app->authManager->createRole('admin');
//    $admin->description = 'Администратор';
//    Yii::$app->authManager->add($admin);
//
//    $user = Yii::$app->authManager->createRole('user');
//    $user->description = 'Пользователь';
//    Yii::$app->authManager->add($user);
//
//    $ban = Yii::$app->authManager->createRole('banned');
//    $ban->description = 'Заблокирован';
//    Yii::$app->authManager->add($ban);

//    $permit = Yii::$app->authManager->createPermission('canAdmin');
//    $permit->description = 'Право на вход в админку';
//    Yii::$app->authManager->add($permit);

//    $role_a = Yii::$app->authManager->getRole('admin');
//    $role_b = Yii::$app->authManager->getRole('user');
//    $role_c = Yii::$app->authManager->getRole('banned');
//    $permit = Yii::$app->authManager->getPermission('canAdmin');
//    Yii::$app->authManager->addChild($role_a, $permit);

//    $userRole = Yii::$app->authManager->getRole('admin');
//    Yii::$app->authManager->assign($userRole, 1);
//    return '132';
//  }

}