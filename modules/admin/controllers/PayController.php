<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 29.08.2017
 * Time: 0:09
 */

namespace app\modules\admin\controllers;
use app\controllers\AppController;
use app\controllers\History;

use app\models\Payment;
use yii\data\Pagination;
use Yii;

class PayController extends AppController
{

  public function actionIndex() {

    $query = History::find()->where([]);
    $countQuery = clone $query;
    $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);

    $pages->pageSizeParam = false;

    $models = $query->offset($pages->offset)
      ->limit($pages->limit)
      ->all();

    $active_str = '';

    foreach ($models as $row) {
      $active_str .= '<tr><td>' . $row['id'] . '</td><td>' . $row['id_user'] . '</td><td>' . $row['date'] . '</td><td>' . $row['bonus'] . '</td></tr>';
    }


    return $this->render('index', [
      'active_str' => $active_str,
      'pages' => $pages,
    ]);

  }

  public function actionOutput()
  {
    $query = Payment::find()->where([]);
    $countQuery = clone $query;
    $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);

    $pages->pageSizeParam = false;

    $models = $query->offset($pages->offset)
      ->limit($pages->limit)
      ->all();

    $active_str = '';

    foreach ($models as $row) {
      $active_str .= '<tr><td>' . $row['id'] . '</td><td>' . $row['id_user'] . '</td><td>' . $row['date'] . '</td><td>' . $row['pay'] . '</td></tr>';
    }


    return $this->render('output', [
      'active_str' => $active_str,
      'pages' => $pages,
    ]);
  }

}