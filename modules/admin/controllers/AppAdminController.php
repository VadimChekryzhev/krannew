<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 18.08.2017
 * Time: 13:27
 */

namespace app\modules\admin\controllers;

use app\controllers\AppController;
//use yii\web\Controller;
use yii\filters\AccessControl;


class AppAdminController extends AppController
{

  public function behaviors()
  {
    return [
      'access' => [
        'class'=> AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'roles' => ['canAdmin'],
          ]
        ]
      ]
    ];
  }

}