<?php

  use yii\widgets\LinkPager;

?>

<h1>Бонусы</h1>

<table>
  <tr>
    <th>№</th>
    <th>ID пользователя</th>
    <th>Date</th>
    <th>Bonus</th>
  </tr>
  <?= $active_str ?>
</table>


<div class="wrap-pagination">

  <?php
    echo LinkPager::widget([
      'pagination' => $pages,
    ]);
  ?>
</div>

