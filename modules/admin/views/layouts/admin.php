<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

<div class="container">
  <div class="wrapper clearfix">
    <div class="col-md-12">
      <div class="content">
        <div class="wrap">
          <ul class="nav nav-pills nav-justified top-menu">
            <li role="presentation"><?= Html::a('Главная', ['/site/index']) ?></li>
            <li role="presentation"><?= Html::a('Админка', ['/admin']) ?></li>
            <li role="presentation"><?= Html::a('Бонусы', ['/admin/pay/index']) ?></li>
            <li role="presentation"><?= Html::a('Выплаты', ['/admin/pay/output']) ?></li>
            <li role="presentation"><?= Html::a('Управление пользователями', ['/admin/users']) ?></li>
<!--            <li role="presentation">--><?//= Html::a('Удвой баланс', ['game/lotery']) ?><!--</li>-->
            <?php if (!Yii::$app->user->isGuest): ?>
<!--              <li role="presentation">--><?//= Html::a('Профиль', ['/user/profile']) ?><!--</li>-->
              <li role="presentation"><?= Html::a('Выход', ['/user/logout']) ?></li>
            <?php else: ?>
              <li role="presentation"><?= Html::a('Вход', ['user/login']) ?></li>
<!--              <li role="presentation">--><?//= Html::a('Регистрация', ['user/registr']) ?><!--</li>-->
            <?php endif; ?>
            <!--            <li role="presentation">--><?//= Html::a('Поддержка', ['support/support']) ?><!--</li>-->
          </ul>
        </div>

        <?= $content ?>
      </div>
    </div>

  </div>

  <footer>
    <p>Тут будет футер</p>
  </footer>

</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
